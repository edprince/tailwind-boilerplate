import React, { useState } from 'react';
import SideBar from './components/SideBar';
import NavBar from './components/NavBar';
import PageHeader from './components/PageHeader';
import { Line } from 'react-chartjs-2';
import { Map, TileLayer, LayersControl, Marker, Popup } from 'react-leaflet';


var data = {
				labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
				datasets: [{
          label: 'My First dataset',
          backgroundColor: "#ff6384",
          borderColor: "#ff6384",
					data: [
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor()
					],
					fill: false,
				}, {
          label: 'My Second dataset',
          backgroundColor: "#36a2eb",
          borderColor: "#36a2eb",
          borderDash: [5, 5],
					fill: false,
					data: [
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor()
					],
				}]
			};

function App() {
  const [lat, setLat] = useState(51.1099575);
  const [lng, setLng] = useState(17.0245239);

  return (
    <div>
      <div className="flex h-screen">
        <div className="hidden md:block md:w-1/3 lg:w-1/4 xl:w-1/6 bg-white shadow p-6 z-10">
          <SideBar />
        </div>
        <div className="overflow-y-scroll w-full md:w-2/3 lg:w-3/4 xl:w-5/6 z-0">
          <NavBar />
          <main className="">
            <div className="max-w-7xl mx-auto py-8 sm:px-6 lg:px-8">
              <div className="px-8 py-4 bg-white shadow rounded">
                  <PageHeader title="Monthly Projections" />
                  <Line data={data} height={80} />
              </div>
            </div>

            <div className="max-w-7xl mx-auto pb-8 sm:px-6 lg:px-8">
              <div className="px-8 py-4 bg-white shadow rounded">
                  <PageHeader title="Teams" />
                    <Map center={[lat, lng]} zoom={3}>
                      <LayersControl position="topright">
                        <TileLayer
                          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                          url="https://tiles.stadiamaps.com/tiles/alidade_smooth/{z}/{x}/{y}{r}.png"
                        />

                      </LayersControl>
                      </Map>
              </div>
            </div>
          </main>
        </div>
      </div>
    </div>
  );
}

function randomScalingFactor() {
  return Math.floor(Math.random() * 20);      // returns a random integer from 0 to 10

}

export default App;
