import React from 'react';

function Button(props) {
  return (
  <button className="ml-4 flex rounded-md text-white block bg-gray-900 hover:bg-gray-800 px-4 py-2">{props.icon}{props.label}</button>
  );
}

export default Button;
