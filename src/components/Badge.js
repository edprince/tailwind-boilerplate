import React from 'react';

function Badge(props) {
  return (
  <span className="text-xs font-bold -mr-2 flex rounded text-green-800 block bg-green-200 px-2 py-1">{props.value}</span>
  );
}

export default Badge;
