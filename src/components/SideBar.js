import React from 'react';
import MenuHeading from './MenuHeading';
import MenuLink from './MenuLink';

function Button() {
  return (
    <div className="flex-shrink-0">
      <img className="ml-3 mb-4 h-8 w-8" src="https://tailwindui.com/img/logos/workflow-mark-on-dark.svg" alt="Workflow logo" />
      <MenuHeading label="Project" />
      <MenuLink link="#" label="Dashboard" />
      <MenuLink link="#" label="Teams" />
      <MenuLink link="#" label="Projects" />
      <MenuLink link="#" label="Calendar" />
      <MenuLink link="#" label="Reports" />

      <MenuHeading label="Account" />
      <MenuLink link="#" label="Teams" />
      <MenuLink link="#" label="Settings" />
      <MenuLink link="#" label="Sign Out" />
    </div>
  );
}

export default Button;
