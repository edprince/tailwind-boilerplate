import React from 'react';

export function NotificationIcon() {
  return (
    <button className="p-1 border-2 border-transparent text-gray-800 rounded-full hover:bg-gray-300 focus:outline-none focus:text-white focus:bg-gray-700" aria-label="Notifications">
      <svg className="h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9" />
      </svg>
    </button>
  );
}

export function Plus() {
  return (
    <span className="text-white">
      <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 icon icon-tabler icon-tabler-plus" viewBox="0 0 24 24" stroke-width="1.5" stroke="cuurrentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
        <line x1="12" y1="5" x2="12" y2="19" />
        <line x1="5" y1="12" x2="19" y2="12" />
      </svg>
    </span>
  )
}