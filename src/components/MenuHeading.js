import React from 'react';

function MenuHeading(props) {
  return (
      <span className="mt-5 flex px-3 py-2 uppercase text-gray-600 font-semibold text-sm">{props.label}</span>
  );
}

export default MenuHeading;
