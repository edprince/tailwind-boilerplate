import React from 'react';
import MenuDropdown from './MenuDropdown';
import SearchBar from './SearchBar';
import Button from './Button';
import Badge from './Badge';
import { NotificationIcon } from './Icons';


function NavBar() {
  return (
        <div className="z-10">
          <nav className="bg-white shadow">
            <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
              <div className="flex items-center justify-between h-16">
                <div className="flex items-center">
                  <div className="hidden md:block">
                    <div className="flex items-baseline space-x-4">
                      <SearchBar />
                    </div>
                  </div>
                </div>
                <div className="hidden md:block">
                  <div className="ml-4 flex items-center md:ml-6">
                      <a href="#" className="px-3 py-2 rounded-md text-sm font-medium text-gray-900 hover:bg-gray-200 focus:outline-none focus:text-white focus:bg-gray-900">Dashboard</a>
                      <a href="#" className="px-3 py-2 rounded-md text-sm font-medium text-gray-900 hover:bg-gray-200 focus:outline-none focus:text-white focus:bg-gray-900">Pricing</a>
                      <a href="#" className="px-3 py-2 rounded-md text-sm font-medium text-gray-900 hover:bg-gray-200 focus:outline-none focus:text-white focus:bg-gray-900">Projects</a>
                      <a href="#" className="px-3 py-2 rounded-md text-sm font-medium text-gray-900 hover:bg-gray-200 focus:outline-none focus:text-white focus:bg-gray-900">Help</a>
                      <NotificationIcon />

                    <div className="ml-3 relative">
                      <div>
                        <button className="max-w-xs flex items-center text-sm rounded-full text-white focus:outline-none focus:shadow-solid" id="user-menu" aria-label="User menu" aria-haspopup="true">
                          <img className="h-8 w-8 rounded-full" src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="" />
                        </button>
                      </div>
                      <MenuDropdown />
                    </div>
                  </div>
                </div>
                <div className="-mr-2 flex md:hidden">
                  <button className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:bg-gray-200 focus:outline-none focus:bg-gray-900">
                    <svg className="block h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                    </svg>
                    <svg className="hidden h-6 w-6" stroke="currentColor" fill="none" viewBox="0 0 24 24">
                      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                  </button>
                </div>
              </div>
            </div>
          </nav>
        </div>
  );
}

export default NavBar;
