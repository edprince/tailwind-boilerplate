import React from 'react';
import Button from './Button';
import { Plus } from './Icons';

function PageHeader(props) {
  return (
        <header className="">
          <div className="flex max-w-7xl border-b pb-4 mb-8">
            <h1 className="text-3xl font-bold leading-tight text-gray-900">
              {props.title}
            </h1>
            <img src="https://randomuser.me/api/portraits/men/32.jpg" className="border-2 border-white rounded-full h-10 ml-4"></img>
            <img src="https://randomuser.me/api/portraits/men/38.jpg" className="border-2 border-white rounded-full h-10 -ml-4"></img>
            <img src="https://randomuser.me/api/portraits/women/32.jpg" className="border-2 border-white rounded-full h-10 -ml-4"></img>
            <Button label="Action"/>
          </div>
        </header>
  );
}

export default PageHeader;