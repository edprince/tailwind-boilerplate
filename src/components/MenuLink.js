import React from 'react';

function MenuLink(props) {
  return (
      <a href={props.link} className="flex px-3 py-2 rounded-md text-sm font-medium text-gray-900 hover:bg-gray-100 focus:outline-none focus:text-white focus:bg-gray-900">{props.label}</a>
  );
}

export default MenuLink;
